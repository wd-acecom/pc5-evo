import CrossoverMethod from "./crossover.js";
import ParentSelectionMethod from "./parentSelection.js";
import NextGenerationSelectionMethod from "./nextGenerationSelection.js";
import { sumator_fit, choice } from "./utils.js";

export default class GA {
  #parents;
  #population;
  #populationSize;
  #currentGeneration;
  #offspringsQuantity;
  #offsprings;
  #mutationProbability;
  #crossoverMethod;
  #parentSelectionMethod;
  #parentSelectionMethodParam;
  #nextGenerationSelectionMethod;
  #nextGenerationSelectionMethodParam;
  #maze;
  #recombinationProbability;
  #recombinationMethodParam;

  #grid;
  #chart;

  constructor({
    populationSize,
    offspringsQuantity,
    mutationProbability,
    recombinationProbability,
    grid,
    crossoverMethod = "n_puntos",
    recombinationMethodParam = 1,
    parentSelectionMethod = "ruleta",
    parentSelectionMethodParam = 1,
    nextGenerationSelectionMethod = "ruleta",
    nextGenerationSelectionMethodParam = 1,
    maze,
  }) {
    this.#populationSize = populationSize;
    this.#population = [];
    this.#parents = [];
    this.#offsprings = [];
    this.#currentGeneration = 0;
    this.#offspringsQuantity = offspringsQuantity;
    this.#mutationProbability = mutationProbability;
    this.#recombinationProbability = recombinationProbability;

    this.#crossoverMethod = crossoverMethod;
    this.#recombinationMethodParam = recombinationMethodParam;

    this.#parentSelectionMethod = parentSelectionMethod;
    this.#parentSelectionMethodParam = parentSelectionMethodParam;

    this.#nextGenerationSelectionMethod = nextGenerationSelectionMethod;
    this.#nextGenerationSelectionMethodParam =
      nextGenerationSelectionMethodParam;

    this.#grid = grid;
    this.#maze = maze;
    this.#chart = null;
  }

  #initializePopulation() {
    for (let i = 0; i < this.#populationSize; i++) {
      let random = Array.from({ length: this.#grid.length ** 2 }, () =>
        Math.floor(Math.random() * 5)
      );

      this.#population.push({
        genotype: random,
        generation: this.#currentGeneration,
      });
    }
  }

  #evaluateFitness(individuals) {
    let evaluated = [];

    let toIterate = individuals;

    toIterate.forEach((individual) => {
      let lastPosition = [0, 0];

      individual.phenotype = individual.genotype.filter(
        (allele) => allele !== 0
      );

      let badMoves = 0;
      let trackingPosition = [];
      let path = [[0, 0]];

      for (let move of individual.phenotype) {
        let walls = this.#grid[lastPosition[0]][lastPosition[1]];

        if (move === 1 && !walls.topWall) lastPosition[0]--;
        else if (move === 2 && !walls.rightWall) lastPosition[1]++;
        else if (move === 3 && !walls.bottomWall) lastPosition[0]++;
        else if (move === 4 && !walls.leftWall) lastPosition[1]--;

        if (trackingPosition.includes(JSON.stringify(lastPosition))) {
          badMoves++;
        } else {
          trackingPosition.push(JSON.stringify(lastPosition));
        }

        path.push([...lastPosition]);
      }

      let zerosQuantity = individual.genotype.filter(
        (allele) => allele === 0
      ).length;

      let multiplier = zerosQuantity / (badMoves ** 2 + 1);

      // let fitness =
      //   (lastPosition[0] ** 2 + lastPosition[1] ** 2) *
      //   trackingPosition.length *
      //   multiplier;

      let fitness =
        (multiplier * Math.sqrt(lastPosition[0] ** 2 + lastPosition[1] ** 2)) /
        Math.sqrt(
          (this.#grid.length - 1 - lastPosition[0]) ** 2 +
            (this.#grid.length - 1 - lastPosition[1]) ** 2
        );

      individual.multiplier = multiplier;
      individual.lastPosition = lastPosition;
      individual.fitness = fitness;
      individual.badMoves = badMoves;
      individual.maxSize = trackingPosition.length;
      individual.path = path;

      evaluated.push(individual);
    });

    return evaluated;
  }

  #evaluatedNewGen(sort_individuals, size) {
    const sum_fitness = sumator_fit(sort_individuals);
    let new_individuals = sort_individuals.map((indiv) => {
      let new_individual = indiv;
      new_individual.selection_probability = indiv.fitness / sum_fitness;
      return new_individual;
    });
    const individuals_choised = choice(
      new_individuals,
      size,
      new_individuals.map((indiv) => indiv.selection_probability)
    );

    return individuals_choised;
  }

  #evaluateFitnessAndSetParams(type = "population") {
    let evaluated = [];

    let toIterate = type === "population" ? this.#population : this.#offsprings;

    toIterate.forEach((individual) => {
      let lastPosition = [0, 0];

      individual.phenotype = individual.genotype.filter(
        (allele) => allele !== 0
      );

      let badMoves = 0;
      let trackingPosition = [];
      let path = [[0, 0]];

      for (let move of individual.phenotype) {
        let walls = this.#grid[lastPosition[0]][lastPosition[1]];

        if (move === 1 && !walls.topWall) lastPosition[0]--;
        else if (move === 2 && !walls.rightWall) lastPosition[1]++;
        else if (move === 3 && !walls.bottomWall) lastPosition[0]++;
        else if (move === 4 && !walls.leftWall) lastPosition[1]--;

        if (trackingPosition.includes(JSON.stringify(lastPosition))) {
          badMoves++;
        } else {
          trackingPosition.push(JSON.stringify(lastPosition));
        }

        path.push([...lastPosition]);
      }

      let yWalls = 0;
      let xWalls = 0;

      for (let i = lastPosition[0]; i < this.#grid.length - 1; i++) {
        if (this.#grid[i][lastPosition[1]].bottomWall) {
          yWalls += 1;
        }
        if (this.#grid[i][lastPosition[1]].leftWall) {
          yWalls += 1;
        }
        if (this.#grid[i][lastPosition[1]].rightWall) {
          yWalls += 1;
        }
        if (this.#grid[i][lastPosition[1]].topWall) {
          yWalls += 1;
        }
      }

      for (let i = lastPosition[1]; i < this.#grid.length - 1; i++) {
        if (this.#grid[this.#grid.length - 1][i].rightWall) {
          xWalls += 1;
        }
        if (this.#grid[this.#grid.length - 1][i].topWall) {
          xWalls += 1;
        }
        if (this.#grid[this.#grid.length - 1][i].leftWall) {
          xWalls += 1;
        }
        if (this.#grid[this.#grid.length - 1][i].bottomWall) {
          xWalls += 1;
        }
      }

      let zerosQuantity = individual.genotype.filter(
        (allele) => allele === 0
      ).length;

      let multiplier =
        (this.#grid.length ** 2 * zerosQuantity) /
        (badMoves ** 3 * (yWalls ** 2 + xWalls ** 2 + 1) ** 2);

      let fitness =
        (multiplier * Math.sqrt(lastPosition[0] ** 2 + lastPosition[1] ** 2)) /
        Math.sqrt(
          (this.#grid.length - 1 - lastPosition[0]) ** 2 +
            (this.#grid.length - 1 - lastPosition[1]) ** 2
        );

      individual.multiplier = multiplier;
      individual.lastPosition = lastPosition;
      individual.fitness = fitness;
      individual.badMoves = badMoves;
      individual.maxSize = trackingPosition.length;
      individual.path = path;

      evaluated.push(individual);
    });

    if (type === "population") {
      this.#population = [...evaluated];
    } else {
      this.#offsprings = [...evaluated];
    }
  }

  #evaluateTerminationCondition() {
    return (
      !(
        this.#population[0].lastPosition[0] === this.#grid.length - 1 &&
        this.#population[0].lastPosition[1] === this.#grid.length - 1
      ) && this.#currentGeneration < 1000
    );
  }

  #selectParents() {
    let parentSelectionMethod = new ParentSelectionMethod(
      this.#parentSelectionMethod,
      [...this.#population],
      // this.#offspringsQuantity,
      Math.floor(this.#population.length * 0.75),
      this.#parentSelectionMethodParam,
      this.#parentSelectionMethodParam
    );

    this.#parents = [...parentSelectionMethod.execute()];
  }

  #recombineParents() {
    // this.#offsprings = [];

    // for (let i = 0; i < this.#parents.length; i += 2) {
    //   let crossoverMethodInstance = new CrossoverMethod(
    //     this.#crossoverMethod,
    //     [this.#parents[i], this.#parents[i + 1]],
    //     2,
    //     1
    //   );

    //   this.#offsprings.push(...crossoverMethodInstance.execute());
    // }

    this.#offsprings = [];
    for (let j = 0; j < this.#offspringsQuantity; j++) {
      let i = 0;
      let parent_selector = [];
      while (i < 2) {
        let parent_index = Math.floor(Math.random() * this.#parents.length);
        if (parent_selector.indexOf(this.#parents[parent_index]) === -1) {
          parent_selector.push(this.#parents[parent_index]);
          i++;
        }
      }
      let crossoverMethodInstance = new CrossoverMethod(
        this.#crossoverMethod,
        parent_selector,
        this.#recombinationMethodParam,
        this.#recombinationProbability
      );
      this.#offsprings.push(...crossoverMethodInstance.execute());
    }
  }

  #mutateOffsprings() {
    this.#offsprings = this.#offsprings.map((offspring) => {
      return {
        genotype: offspring.genotype.map((allele) => {
          if (Math.random() < this.#mutationProbability) {
            allele = (allele + (Math.random() <= 0.5 ? 1 : -1)) % 5;
            // return Math.floor(Math.random() * 5);
            return allele < 0 ? 4 : allele;
          }

          return allele;
        }),
      };

      // if (Math.random() < this.#mutationProbability) {
      //   let chain_iterator = Math.floor(
      //     Math.random() * (offspring.genotype.length - 3)
      //   );
      //   let chain_length = Math.floor(
      //     Math.random() * (offspring.genotype.length - chain_iterator - 1) + 3
      //   );
      //   let sub_genotype = offspring.genotype.slice(
      //     chain_iterator,
      //     chain_iterator + chain_length
      //   );
      //   let case_genotype = [];
      //   for (let i = 0; i < 3; i++) {
      //     let _mixed_sub_genotype = sub_genotype
      //       .map((_, i) => [Math.random(), i])
      //       .sort(([a], [b]) => a - b)
      //       .map(([, i]) => sub_genotype[i]);
      //     let genotype_init = offspring.genotype.slice(0, chain_iterator);
      //     let genotype_end = offspring.genotype.slice(
      //       chain_iterator + chain_length,
      //       offspring.genotype.length
      //     );
      //     let new_genotype = genotype_init
      //       .concat(_mixed_sub_genotype)
      //       .concat(genotype_end);
      //     let individual = {
      //       genotype: new_genotype,
      //     };
      //     case_genotype.push(individual);
      //   }
      //   case_genotype = this.#evaluateFitness(case_genotype).sort(
      //     (a, b) => b.fitness - a.fitness
      //   );
      //   return case_genotype[0];
      // } else {
      //   return offspring;
      // }
    });
  }

  #selectNextGeneration() {
    this.#currentGeneration += 1;
    this.#population = this.#population.filter(
      (individual_) => this.#currentGeneration - individual_.generation < 50
    );
    this.#population.push(...this.#offsprings);

    let nextGenerationSelectionMethod = new NextGenerationSelectionMethod(
      this.#nextGenerationSelectionMethod,
      [...this.#population],
      this.#evaluatedNewGen,
      this.#populationSize,
      this.#nextGenerationSelectionMethodParam
    );

    this.#population = [...nextGenerationSelectionMethod.execute()];

    this.#population.sort((a, b) => b.fitness - a.fitness);

    this.#population = this.#population.map((individual_) => {
      let individual = { ...individual_ };
      if (
        individual.generation === null ||
        individual.generation === undefined
      ) {
        individual.generation = this.#currentGeneration;
      }

      return {
        genotype: individual.genotype,
        generation: individual.generation,
        phenotype: individual.phenotype,
        lastPosition: individual.lastPosition,
        fitness: individual.fitness,
        multiplier: individual.multiplier,
        badMoves: individual.badMoves,
        maxSize: individual.maxSize,
        path: individual.path,
      };
    });
  }

  #plot(data) {
    document.getElementById("gaChart").remove();
    let canvas = document.createElement("canvas");
    canvas.setAttribute("id", "gaChart");

    document.getElementById("chartContainer").appendChild(canvas);

    let chartCtx = document.getElementById("gaChart").getContext("2d");
    document.getElementById("chartContainer").style.display = "block";

    this.#chart = new Chart(chartCtx, {
      type: "line",
      data: {
        labels: Array.from(
          { length: this.#currentGeneration },
          (_, i) => i + 1
        ),
        datasets: [
          {
            label: "fitness vs generation",
            data: data,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
            ],
            borderColor: [
              "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }

  run() {
    let data = [];
    let bestIndividual = [];

    this.#initializePopulation();
    this.#evaluateFitnessAndSetParams();

    while (this.#evaluateTerminationCondition()) {
      this.#selectParents();
      this.#recombineParents();
      this.#mutateOffsprings();
      this.#evaluateFitnessAndSetParams("offsprings");
      this.#selectNextGeneration();

      if (bestIndividual.length === 0) {
        bestIndividual.push(this.#population[0]);
      } else {
        if (bestIndividual[0].fitness < this.#population[0].fitness) {
          bestIndividual = [this.#population[0]];
        }
      }

      data.push(JSON.parse(JSON.stringify(this.#population[0].fitness)));
    }

    if (this.#currentGeneration <= 1000) {
      console.log(
        "Best value",
        this.#currentGeneration,
        bestIndividual[0].lastPosition,
        bestIndividual[0].fitness,
        bestIndividual[0].phenotype
      );
    }

    this.#maze.highlightPath(bestIndividual[0].path);
    this.#plot(data);
  }
}
