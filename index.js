"use strict";
import GA from "./ga.js";
import Maze from "./maze.js";

let maze = new Maze(700, 7);
maze.draw();

function runGA() {
  let populationSize = Number(document.getElementById("populationSize").value);
  let offspringsQuantity = Number(
    document.getElementById("offspringsQuantity").value
  );
  let mutationProbability = Number(
    document.getElementById("mutationProbability").value
  );
  let recombinationProbability = Number(
    document.getElementById("recombinationProbability").value
  );
  let grid = maze.getReduceGrid();
  let crossoverMethod = document.getElementById("crossoverMethod").value;
  let recombinationMethodParam = Number(
    document.getElementById("crossoverMethodParam").value
  );

  let parentSelectionMethod = document.getElementById(
    "parentSelectionMethod"
  ).value;
  let parentSelectionMethodParam = Number(
    document.getElementById("parentSelectionMethodParam").value
  );

  let nextGenerationSelectionMethod = document.getElementById(
    "nextGenerationSelectionMethod"
  ).value;
  let nextGenerationSelectionMethodParam = Number(
    document.getElementById("nextGenerationSelectionMethodParam").value
  );

  let config = {
    populationSize,
    offspringsQuantity,
    mutationProbability,
    recombinationProbability,
    grid,
    crossoverMethod,
    recombinationMethodParam,
    parentSelectionMethod,
    parentSelectionMethodParam,
    nextGenerationSelectionMethod,
    nextGenerationSelectionMethodParam,
    maze,
  };

  let ga = new GA(config);
  ga.run();
}

window.runGA = runGA;
