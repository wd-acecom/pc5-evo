export function generateRandomIntBetween(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

export function choice(events, size, probability, repetition = true) {
  let toBeSelected = [...events];
  let choices = [];

  while (choices.length < size || toBeSelected.length < 0) {
    toBeSelected.sort(
      (a, b) => b.selection_probability - a.selection_probability
    );

    let random = Math.random() * toBeSelected[0].selection_probability;

    let selected = toBeSelected.filter(
      (ind) => ind.selection_probability >= random
    );

    choices.push(...selected);
    toBeSelected = toBeSelected.filter((ind) => !choices.includes(ind));
  }

  return choices;
}

export function minimum(events) {
  return events.reduce((prev, curr) => {
    return prev.fitness < curr.fitness ? prev : curr;
  });
}

export function sumator_fit(events) {
  let initialValue = 0;
  return events.reduce((prev, curr) => {
    return prev + curr.fitness;
  }, initialValue);
}

export function choise_samples(events, k = 1, repetition = false) {
  let choises = [];
  let i = 0;
  while (i < k) {
    let event = events[Math.floor(Math.random() * (events.length - 1))];
    if (!repetition) {
      if (choises.indexOf(event) === -1) {
        choises.push(event);
        i++;
      }
    } else {
      choises.push(event);
      i++;
    }
  }
  return choises;
}

export function samples_array(size, k = 2) {
  let choises = [];
  let i = 0;
  while (i < k) {
    let event = Math.floor(Math.random() * size);
    choises.push(event);
    i++;
  }
  return choises;
}
