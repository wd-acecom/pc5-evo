export function removeWalls(firstCell, secondCell) {
  const x = firstCell.columnIndex - secondCell.columnIndex;
  const y = firstCell.rowIndex - secondCell.rowIndex;

  if (x === 1) {
    firstCell.walls.leftWall = false;
    secondCell.walls.rightWall = false;
  } else if (x === -1) {
    firstCell.walls.rightWall = false;
    secondCell.walls.leftWall = false;
  }

  if (y === 1) {
    firstCell.walls.topWall = false;
    secondCell.walls.bottomWall = false;
  } else if (y === -1) {
    firstCell.walls.bottomWall = false;
    secondCell.walls.topWall = false;
  }
}

export default class Cell {
  #parentGrid;
  #parentSize;
  #ctx;

  constructor(rowIndex, columnIndex, parentGrid, parentSize, ctx) {
    this.#parentGrid = parentGrid;
    this.#parentSize = parentSize;
    this.goal = false;

    this.rowIndex = rowIndex;
    this.columnIndex = columnIndex;
    this.visited = false;

    this.walls = {
      topWall: true,
      rightWall: true,
      bottomWall: true,
      leftWall: true,
    };

    this.#ctx = ctx;
  }

  checkNeighbours() {
    const rowIndex = this.rowIndex;
    const columnIndex = this.columnIndex;
    const neighbours = [];

    const top =
      rowIndex !== 0 ? this.#parentGrid[rowIndex - 1][columnIndex] : undefined;
    const right =
      columnIndex !== this.#parentGrid.length - 1
        ? this.#parentGrid[rowIndex][columnIndex + 1]
        : undefined;
    const bottom =
      rowIndex !== this.#parentGrid.length - 1
        ? this.#parentGrid[rowIndex + 1][columnIndex]
        : undefined;
    const left =
      columnIndex !== 0
        ? this.#parentGrid[rowIndex][columnIndex - 1]
        : undefined;

    if (top && !top.visited) neighbours.push(top);
    if (right && !right.visited) neighbours.push(right);
    if (bottom && !bottom.visited) neighbours.push(bottom);
    if (left && !left.visited) neighbours.push(left);

    if (neighbours.length !== 0) {
      const random = Math.floor(Math.random() * neighbours.length);
      return neighbours[random];
    } else {
      return undefined;
    }
  }

  #drawTopWall(x, y, n) {
    this.#ctx.beginPath();
    this.#ctx.moveTo(x, y);
    this.#ctx.lineTo(x + this.#parentSize / n, y);
    this.#ctx.stroke();
  }

  #drawRightWall(x, y, n) {
    this.#ctx.beginPath();
    this.#ctx.moveTo(x + this.#parentSize / n, y);
    this.#ctx.lineTo(x + this.#parentSize / n, y + this.#parentSize / n);
    this.#ctx.stroke();
  }

  #drawBottomWall(x, y, n) {
    this.#ctx.beginPath();
    this.#ctx.moveTo(x, y + this.#parentSize / n);
    this.#ctx.lineTo(x + this.#parentSize / n, y + this.#parentSize / n);
    this.#ctx.stroke();
  }

  #drawLeftWall(x, y, n) {
    this.#ctx.beginPath();
    this.#ctx.moveTo(x, y);
    this.#ctx.lineTo(x, y + this.#parentSize / n);
    this.#ctx.stroke();
  }

  highlight(columns) {
    let x = (this.columnIndex * this.#parentSize) / columns + 1;
    let y = (this.rowIndex * this.#parentSize) / columns + 1;

    this.#ctx.fillStyle = "purple";
    this.#ctx.fillRect(
      x,
      y,
      this.#parentSize / columns - 3,
      this.#parentSize / columns - 3
    );
  }

  show(n) {
    const x = (this.columnIndex * this.#parentSize) / n;
    const y = (this.rowIndex * this.#parentSize) / n;

    this.#ctx.strokeStyle = "#ffffff";
    this.#ctx.fillStyle = "black";
    this.#ctx.lineWidth = 2;

    if (this.walls.topWall) this.#drawTopWall(x, y, n);
    if (this.walls.rightWall) this.#drawRightWall(x, y, n);
    if (this.walls.bottomWall) this.#drawBottomWall(x, y, n);
    if (this.walls.leftWall) this.#drawLeftWall(x, y, n);
    if (this.visited) {
      this.#ctx.fillRect(
        x + 1,
        y + 1,
        this,
        this.#parentSize / n - 2,
        this,
        this.#parentSize / n - 2
      );
    }
    if (this.goal) {
      this.#ctx.fillStyle = "rgb(83, 247, 43)";
      this.#ctx.fillRect(
        x + 1,
        y + 1,
        this.#parentSize / n - 2,
        this.#parentSize / n - 2
      );
    }
  }
}
