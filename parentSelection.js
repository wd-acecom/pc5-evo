import { choice, sumator_fit } from "./utils.js";

export default class Selection_GA {
  constructor(_name, _indiv, _size = 1, _s = 1, _k = 1) {
    this._name = _name;
    this._indiv = _indiv;
    this._size = _size;
    this._s = _s;
    this._k = _k;
  }

  execute() {
    switch (this._name) {
      case "ruleta":
        return this.roulette();
      case "ranking lineal":
        return this.lineal_rank();
      case "torneo":
        return this.tournament();
      case "uniforme":
        return this.uniform();
      case "boltzman":
        return this.boltzman();
      default:
        return this.roulette();
    }
  }

  roulette() {
    const sum_fitness = sumator_fit(this._indiv);
    let new_individuals = this._indiv.map((indiv) => {
      let new_individual = indiv;
      new_individual.selection_probability = indiv.fitness / sum_fitness;
      return new_individual;
    });
    const individuals_choised = choice(
      new_individuals,
      this._size,
      new_individuals.map((indiv) => indiv.selection_probability)
    );

    return individuals_choised;
  }

  tournament() {
    let individuals_choised = [];
    let j = 0;
    while (j < this._size) {
      let b_id = -1;
      for (let i = 0; i < this._k; i++) {
        let id = Math.floor(Math.random() * this._indiv.length);
        if (b_id == -1 || this._indiv[b_id].fitness < this._indiv[id].fitness)
          b_id = id;
      }
      if (individuals_choised.indexOf(this._indiv[b_id]) === -1) {
        individuals_choised.push(this._indiv[b_id]);
        j++;
      }
    }

    return individuals_choised;
  }

  lineal_rank() {
    let new_individuals = this._indiv.sort((a, b) => b.fitness - a.fitness);
    let mu = new_individuals.length;
    for (let i = 0; i < mu; i++) {
      new_individuals[i].selection_probability =
        (2 - this._s) / mu + 2 * i * ((this._s - 1) / (mu * (mu - 1)));
    }
    const individuals_choised = choice(
      new_individuals,
      this._size,
      new_individuals.map((indiv) => indiv.selection_probability)
    );
    return individuals_choised;
  }

  uniform() {
    const sum_fitness = sumator_fit(this._indiv);
    let new_individuals = this._indiv.map((indiv) => {
      let new_individual = indiv;
      new_individual.selection_probability = 1 / sum_fitness;
      return new_individual;
    });

    const individuals_choised = choice(
      new_individuals,
      this._size,
      new_individuals.map((indiv) => indiv.selection_probability)
    );

    return individuals_choised;
  }
}
