import Cell, { removeWalls } from "./cell.js";

export default class Maze {
  #canvasSize;
  #n;
  #grid;
  #stack;
  #current;
  #generationComplete;
  #canvas;
  #ctx;

  constructor(size, n) {
    this.#canvasSize = size;
    this.#n = n;
    this.#grid = [];
    this.#stack = [];
    this.#generationComplete = false;

    this.#canvas = document.querySelector(".maze");
    this.#ctx = this.#canvas.getContext("2d");

    this.#setup();
  }

  #setup() {
    for (let r = 0; r < this.#n; r++) {
      let row = [];

      for (let c = 0; c < this.#n; c++) {
        row.push(new Cell(r, c, this.#grid, this.#canvasSize, this.#ctx));
      }

      this.#grid.push(row);
    }

    this.#current = this.#grid[0][0];
    this.#grid[this.#n - 1][this.#n - 1].goal = true;

    this.#current.visited = true;

    while (!this.#generationComplete) {
      let nextCell = this.#current.checkNeighbours();

      if (nextCell) {
        nextCell.visited = true;
        this.#stack.push(nextCell);
        removeWalls(this.#current, nextCell);
        this.#current = nextCell;
      } else if (this.#stack.length > 0) {
        const cell = this.#stack.pop();
        this.#current = cell;
      }

      if (this.#stack.length === 0) {
        this.#generationComplete = true;
        return;
      }
    }
  }

  draw() {
    this.#canvas.width = this.#canvasSize;
    this.#canvas.height = this.#canvasSize;
    this.#canvas.style.background = "black";

    for (let r = 0; r < this.#n; r++) {
      for (let c = 0; c < this.#n; c++) {
        this.#grid[r][c].show(this.#n);
      }
    }
  }

  highlightPath(path) {
    let indexes = path.shift();

    if (indexes === null || indexes === undefined) {
      return;
    }

    this.draw();
    this.#grid[indexes[0]][indexes[1]].highlight(this.#n);

    window.requestAnimationFrame(() => {
      this.highlightPath(path);
    });
  }

  getReduceGrid() {
    const reducedGrid = Array(this.#n)
      .fill()
      .map(() => Array(this.#n).fill(0));

    const cells = this.#grid
      .flatMap((cell) => cell)
      .map(({ rowIndex, columnIndex, walls }) => {
        return { rowIndex, columnIndex, walls };
      });

    cells.forEach((cell) => {
      reducedGrid[cell.rowIndex][cell.columnIndex] = cell.walls;
    });

    return reducedGrid;
  }
}
